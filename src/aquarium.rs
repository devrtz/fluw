use crate::fluw;
use crate::fluw::Flows;

use bevy::app;
use bevy::ecs::{ Mut, Query, With };
use bevy::math::Vec3;
use bevy::transform::components::{ Parent, Transform };
use nalgebra::geometry::{ Point2, Translation2 };


use bevy::ecs::IntoSystem;


pub struct Floater;

pub struct Aquarium {
    pub flows: Flows,
}

fn to_shape(width: usize, height: usize, pos: Point2<f32>) -> Point2<f32> {
        let width = width as f32;
        let height = height as f32;
        let newposx = pos.x * width + width / 2.0 - 0.5;
        let newposy = pos.y * width + height / 2.0 - 0.5;
        Point2::new(newposx, newposy)
}

impl Aquarium {
    /// Expects x dimension betwen -1; 1, y centered on 0.
    fn flow_at(&self, pos: Point2<f32>) -> Translation2<f32> {
        let shape = self.flows.shape();
        fluw::flow_at(&self.flows, to_shape(shape[0], shape[1], pos))
    }
}

pub struct Plugin;

impl app::Plugin for Plugin {
    fn build(&self, app: &mut app::AppBuilder) {
        app.add_system(float.system());
    }
}

fn float(
    aquaria: Query<&Aquarium>,
    mut floaters: Query<(Mut<Transform>, &Parent), With<Floater>>,
) {
    for (mut transform, parent) in floaters.iter_mut() {
        if let Ok(aquarium) = aquaria.get(**parent) {
            let position = transform.translation;
            let flow = aquarium.flow_at(Point2::new(position.x, position.y));
            transform.translation += Vec3::new(flow.x, flow.y, 0.0);
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use nalgebra::Complex;

    #[test]
    fn shaping() {
        assert_eq!(to_shape(2, 2, Point2::new(0.0, 0.0)), Point2::new(0.5, 0.5));
        assert_eq!(to_shape(2, 2, Point2::new(0.5, 0.5)), Point2::new(1.5, 1.5));
        assert_eq!(to_shape(2, 2, Point2::new(-0.5, -0.5)), Point2::new(-0.5, -0.5));
    }

    #[test]
    fn bounds() {
        let aq = Aquarium {
            flows: Flows::zeros((2, 2)) + Complex::new(1.0, 1.0),
        };
        let null = Translation2::new(0.0, 0.0);
        assert_eq!(aq.flow_at(Point2::new(0.6, 0.0)), null);
        assert_eq!(aq.flow_at(Point2::new(0.6, 0.0)), null);
        assert_eq!(aq.flow_at(Point2::new(-0.6, 0.0)), null);
        assert_eq!(aq.flow_at(Point2::new(0.0, -0.6)), null);
        assert_eq!(aq.flow_at(Point2::new(0.0, 0.6)), null);
    }
}
