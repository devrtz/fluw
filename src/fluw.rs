/*! This approach doesn't work. See tests.
It's still kept here because of some procedures which are useful.
*/

use nalgebra::Complex;
use nalgebra::geometry::{ Point2, Translation2 };
use ndarray::{ Array2, ArrayView2, ArrayViewMut2, s };


use nalgebra::Normed;


/// Real part: first index (row), imaginary part: second index (column).
pub type Flows = Array2<Complex<f32>>;

type FlowsSliceMut<'a> = ArrayViewMut2<'a, Complex<f32>>;
type FlowsSlice<'a> = ArrayView2<'a, Complex<f32>>;


/// Returns interpolated flow at point. x = real, y = imaginary
pub fn flow_at(flows: &Flows, pos: Point2<f32>) -> Translation2<f32>{
    // This still counts as interpolation, right?
    let pos = Point2::new(pos.x.round(), pos.y.round());
    if pos.x < 0.0 || pos.y < 0.0 {
        return Translation2::new(0.0, 0.0);
    }
    let row = pos.x.round() as usize;
    let column = pos.y.round() as usize;

    match flows.get((row, column)) {
        Some(flow) => Translation2::new(flow.re, flow.im),
        None => Translation2::new(0.0, 0.0),
    }
}


// could use ArrayView for steps to avoid allocating
fn flow_seed() -> Flows {
    Flows::zeros((1, 1))
}

fn divide(flows: Flows) -> Flows {
    let shape = flows.shape();
    let shape = (shape[0] * 2, shape[1] * 2);
    Flows::from_shape_fn(shape, |(i, j)| flows[(i / 2, j / 2)])
}

/// Applies curl to 4 neighbors
fn make_stir(magnitude: f32) -> Flows {
    fn c(a: f32, b: f32) -> Complex<f32> {
        Complex::new(a, b)
    }
    // May not really be anticlockwise.
    // Irrelevant anyway because the final array can be presented in any flip.
    // What matters is consistency while processing.
    let anticlockwise = Flows::from(vec![
        // row 0: always previous column
        [c(1.0, -1.0), c(-1.0, -1.0)],
        // row 1: always next column
        [c(1.0, 1.0), c(-1.0, 1.0)],
    ]);
    // Elements of the above are not unit length, need adjusting.
    let norm = c(1.0, 1.0).norm();
    anticlockwise * (magnitude / norm)
}

fn stir_slice(mut flows: FlowsSliceMut, f: impl Fn(Index) -> f32) {
    for (y, mut slice) in flows.exact_chunks_mut((2, 2)).into_iter().enumerate() {
        let index = Index { a: 0, b: 0 }; // FIXME ofc
        slice += &make_stir(f(index));
    }
}

pub struct Index {
    pub a: usize,
    pub b: usize,
}

fn gensplit(mut flows: Flows, i: u8, f: &impl Fn(u8, Index) -> f32) -> Flows {
    // No windows_mut() exists, so do it in 4 steps.
    stir_slice(flows.slice_mut(s![.., ..]), |index| f(i, index));
    stir_slice(
        flows.slice_mut(s![1.., ..]),
        |index| f(i, Index { a: 1 + index.a, b: index.b }),
    );
    stir_slice(
        flows.slice_mut(s![.., 1..]),
        |index| f(i, Index { a: index.a, b: 1 + index.b }),
    );
    stir_slice(
        flows.slice_mut(s![1.., 1..]),
        |index| f(i, Index { a: 1 + index.a, b: 1 + index.b }),
    );
    flows
}

/// Generates a flowing square array
pub fn generate(split_count: u8, f: &impl Fn(u8, Index) -> f32) -> Flows {
    let mut flows = flow_seed();
    for i in 0..split_count {
        flows = divide(flows);
        flows = gensplit(flows, i, f);
    }
    flows
}

/// Generators of concrete flow fields
pub mod stirs {
    use super::Index;

    /// Really basic random integer generator.
    pub fn tausworthe(mut seed: usize) -> usize {
      seed ^= seed >> 13;
      seed ^= seed << 18;
      seed & 0x7fffffff
    }

    pub fn random_basic(iter: u8, index: Index) -> f32 {
        0.001 / (tausworthe(iter as usize * index.a + index.b as usize) as f32 + 1.0)
    }

    pub fn const_one(_iter: u8, _index: Index) -> f32 {
        1.0
    }
    
    pub fn const_one_edge(_iter: u8, index: Index) -> f32 {
        if index.a == 0 {
            0.0
        } else {
            1.0
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    
    use nalgebra::geometry::Rotation2;
    
    /// Rotate by 45 degrees foo-wise.
    fn rot(p: Complex<f32>) -> Complex<f32> {
        let p = Point2::new(p.re, p.im);
        let o = Rotation2::new(std::f32::consts::TAU / 8.0).transform_point(&p);
        Complex::new(o.coords[0], o.coords[1])
    }
    
    /// Not to scale. We only want to compare to 0 anyway.
    /// Should be =0 for every piece, fluid does not compress
    fn divergence2_2(neighbors: FlowsSlice) -> f32 {
        // Rate of flow change ("derivative") over the row axis.
        // Maybe divide by 2. Not sure about signs.
        let dmatching // both axes increase together
            = rot(neighbors[(0, 0)]).im - rot(neighbors[(1, 1)]).im;
        let dopposite // both axes increase opposite
            = rot(neighbors[(0, 1)]).re - rot(neighbors[(1, 0)]).re;
        -(dmatching + dopposite) / c(1.0, 1.0).norm()
    }

    #[derive(Debug, PartialEq)]
    enum DivergenceResult {
        Zero,
        Present,
    }
    /// Checks each 2x2 for having any divergence
    fn divergence_check_squares(flows: FlowsSlice) -> DivergenceResult {
        fn check_offset(sub: FlowsSlice) -> DivergenceResult {
            for slice in sub.windows((2, 2)) {
                if divergence2_2(slice).abs() > 1e-6 {
                    println!("{:#?}", slice);
                    println!("div {}", divergence2_2(slice));
                    return DivergenceResult::Present;
                }
            }
            DivergenceResult::Zero
        }
        
        if let DivergenceResult::Present = check_offset(flows.slice(s![.., ..])) {
            return DivergenceResult::Present;
        }
        if let DivergenceResult::Present = check_offset(flows.slice(s![1.., ..])) {
            return DivergenceResult::Present;
        }
        if let DivergenceResult::Present = check_offset(flows.slice(s![.., 1..])) {
            return DivergenceResult::Present;
        }
        if let DivergenceResult::Present = check_offset(flows.slice(s![1.., 1..])) {
            return DivergenceResult::Present;
        }
        DivergenceResult::Zero
    }

    fn c(a: f32, b: f32) -> Complex<f32> {
        Complex::new(a, b)
    }
    
    #[test]
    fn test_rot() {
        assert_eq!(rot(c(1.0, 1.0)), c(0.0, 1.4142135));
    }
    
    #[test]
    fn test_div() {
        assert_eq!(divergence2_2(Flows::zeros((2, 2)).slice(s![.., ..])), 0.0)
    }
    
    #[test]
    fn test_div_stir() {
        let stir = make_stir(1.0);
        assert_eq!(divergence2_2(stir.slice(s![.., ..])), 0.0)
    }
    
    #[test]
    fn test_div_source() {
        let source = Flows::from(vec![
            [c(-1.0, -1.0), c(0.0, 0.0)],
            [c(0.0, 0.0), c(1.0, 1.0)],
        ]);
        assert_eq!(divergence2_2(source.slice(s![.., ..])), 2.0)
    }

    #[test]
    fn test_div_source2() {
        let source = Flows::from(vec![
            [c(-1.0, 0.0), c(0.0, -1.0)],
            [c(0.0, 1.0), c(1.0, 0.0)],
        ]);
        // TODO: this looks rather wrong, each point points away from the middle
        assert_eq!(divergence2_2(source.slice(s![.., ..])), 0.0)
    }

    #[test]
    fn test_div_source3() {
        let source = Flows::from(vec![
            [c(-1.0, -1.0), c(0.0, 0.0)],
            [c(0.0, 0.0), c(0.0, 0.0)],
        ]);
    
        assert_eq!(divergence2_2(source.slice(s![.., ..])), 1.0)
    }

    #[test]
    fn test_div_flow() {
        let source = Flows::from(vec![
            [c(-1.0, 0.0), c(-1.0, 0.0)],
            [c(-1.0, 0.0), c(-1.0, 0.0)],
        ]);
        assert_eq!(divergence2_2(source.slice(s![.., ..])), 0.0)
    }
    
    #[test]
    fn test_stir() {
        assert_eq!(make_stir(1.0), Flows::zeros((2, 2))); // FIXME: match approximately
    }
    #[test]
    fn test_simple_generate2() {
        assert_eq!(generate(1, &stirs::const_one), Flows::zeros((2, 2))); // FIXME
    }
    #[test]
    fn test_simple_generate4() {
        assert_eq!(generate(2, &stirs::const_one), Flows::zeros((2, 2))); // FIXME
    }
    #[test]
    fn test_simple_generate_bigger() {
        assert_eq!(divergence_check_squares(generate(2, &stirs::const_one).slice(s![.., ..])), DivergenceResult::Zero);
    }
    
    /// This will never work! Egad.
    /// Splitting cells is not a naive operation where you can just repeat the same value.
    /// Divergence may check out on a grand scale, but small scale, there will be farts.
    #[test]
    fn test_stir_3x2_const() {
        let flows = Flows::zeros((3, 2));
        let stirred = gensplit(flows, 2, &stirs::const_one);
        assert_eq!(
            divergence_check_squares(stirred.slice(s![.., ..])),
            DivergenceResult::Zero,
        );
        println!("{:?}", stirred);
        let split = divide(stirred);
        assert_eq!(
            divergence_check_squares(split.slice(s![.., ..])),
            DivergenceResult::Zero,
        );
    }

    #[test]
    fn test_stir_3x3_edge() {
        let flows = Flows::zeros((3, 3));
        let stirred = gensplit(flows, 2, &stirs::const_one_edge);
        assert_eq!(
            divergence_check_squares(stirred.slice(s![.., ..])),
            DivergenceResult::Zero,
        );
        println!("{:?}", stirred);
        let split = divide(stirred);
        assert_eq!(
            divergence_check_squares(split.slice(s![.., ..])),
            DivergenceResult::Zero,
        );
    }
    
    #[test]
    fn test_stir_8x8_const() {
        let flows = Flows::zeros((8, 8));
        assert_eq!(
            divergence_check_squares(gensplit(flows, 3, &stirs::const_one).slice(s![.., ..])),
            DivergenceResult::Zero,
        );
    }

    #[test]
    fn test_more_generate_bigger() {
        assert_eq!(divergence_check_squares(generate(3, &stirs::const_one).slice(s![.., ..])), DivergenceResult::Zero);
    }

    #[test]
    fn flow_bounds() {
        let ones = Flows::zeros((2, 2)) + c(1.0, 1.0);
        assert_eq!(flow_at(&ones, Point2::new(-0.6, 0.0)), Translation2::new(0.0, 0.0));
    }
}
