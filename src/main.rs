use bevy;
use bevy::DefaultPlugins;
use bevy::app::App;
use bevy::asset::{ Assets, AssetServer };
use bevy::ecs::{ Commands, Res, ResMut };
use bevy::math::{ Vec2, Vec3 };
use bevy::prelude::BuildChildren;
use bevy::render::color::Color;
use bevy::render::entity::Camera2dBundle;
use bevy::render::pass::ClearColor;
use bevy::sprite::{ ColorMaterial, Sprite };
use bevy::sprite::entity::SpriteBundle;
use bevy::transform::components::Transform;
use bevy::ui::entity::CameraUiBundle;
use bevy::window::WindowDescriptor;


use bevy::prelude::IntoSystem;


mod aquarium;
mod floow;
mod fluw;


const CAMERA_SCALE: f32 = 1.0;

fn main() {
    App::build()
        .add_plugins(DefaultPlugins)
        .add_resource(WindowDescriptor {
            title: "Fluw".to_string(),
            width: 300.0,
            height: 300.0,
            ..Default::default()
        })
        .add_resource(ClearColor(Color::rgb_u8(5, 5, 10)))
        .add_startup_system(setup.system())
        .add_startup_system(make_aquarium.system())
        .add_plugin(aquarium::Plugin)
        .run();
}

/// UiCamera and Camera2d are spawn once and for all.
/// Despawning them does not seem to be the way to go in bevy.
pub fn setup(
    commands: &mut Commands,
) {
    commands
        .spawn(Camera2dBundle {
            transform: Transform::from_scale(Vec3::splat(CAMERA_SCALE)),
            ..Default::default()
        })
        .spawn(CameraUiBundle::default());
}

pub fn make_aquarium(
    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    //asset_server: Res<AssetServer>,
) {
    let flows = floow::generate(
        4,
        floow::stirs::random_basic,
    );
    commands
        .spawn(SpriteBundle {
            transform: {
                Transform::from_translation(Vec3::new(0.0, 0.0, -10.0))
                    .mul_transform(Transform::from_scale(Vec3::new(300.0, 300.0, 1.0)))
            },
            material: materials.add(Color::MIDNIGHT_BLUE.into()),
            sprite: Sprite {
                size: Vec2::new(1.0, 1.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .with(aquarium::Aquarium {
            flows,
        })
        .with_children(|parent| {
            for x in -10..10 {
                for y in -10..10 {
                    parent
                        .spawn(SpriteBundle {
                            transform: Transform::from_translation(Vec3::new((x as f32 + 0.5) / 20.0, (y as f32 + 0.5) / 20.0, 1.0)),
                            material: materials.add(Color::AZURE.into()),
                            sprite: Sprite {
                                size: Vec2::new(0.01, 0.01),
                                ..Default::default()
                            },
                            ..Default::default()
                        })
                        .with(aquarium::Floater);
                }
            }
        });
}
